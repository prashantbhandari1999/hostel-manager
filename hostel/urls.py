from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, \
    PasswordResetCompleteView
from django.urls import path
from . import views
from hostel.admin import user_admin_site

app_name = 'hostel'

urlpatterns = [
    path('welcome/', views.welcome, name='welcome'),
    path('login', views.signin, name='login'),
    path('register', views.reg, name='reg'),
    path('user/', views.open_dashboard_view, name='dashboard'),
    path('', views.home, name='home'),
    path('verification/<username>/<uid>', views.verification_view, name='verification'),
    path('changepassword', views.change_password, name='changepassword'),
    path('postlogin', views.postlogin, name='postlogin'),
    path('logout', views.logout_view, name='logout'),
    path('user-admin', user_admin_site.urls, name='admin_login'),
    path('update',views.update,name='update'),

]
