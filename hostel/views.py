from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm
from django.core.mail import send_mail
from django.forms import model_to_dict
from django.http import Http404
from django.utils.crypto import get_random_string

from .models import Person
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, update_session_auth_hash, logout
from django.contrib.auth.models import auth
from django.urls import reverse_lazy
from django.views.generic import View
from django.views import generic
from .forms import UserLogin, RegForm, EditProfileForm
from .models import Person
from H_Management.settings import EMAIL_HOST_USER


def welcome(request):
    return render(request, 'hostel/welcome.html', None)


class Login(View):
    form_class = UserLogin
    template_name = 'hostel/userLogin.html'

    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'UserForm': form})

    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            # user = form.save(commit=False)
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            user = authenticate(email=email, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('hostel:welcome')

        return render(request, self.template_name, {'form': form})


def reg(request):
    if request.method == 'POST':
        form = RegForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            phone = form.cleaned_data['phone']
            gender = form.cleaned_data['gender']
            prn = form.cleaned_data['prn']

            cu = User.objects.values_list('email')
            for em in cu:
                if email == em:
                    return redirect('hostel:reg')
            cu = User.objects.values_list('username')
            for em in cu:
                if email == em:
                    return redirect('hostel:reg')
            uid = get_random_string(length=32)

            p = Person()
            p.person_phone = phone
            p.gender = gender
            p.person_prn = prn
            p.UID = uid
            message = "hostelmanagerintern.herokuapp.com/verification/" + str(form.cleaned_data['email']) + "/" + uid
            send_mail("Verification/", message, EMAIL_HOST_USER, [form.cleaned_data['email']], fail_silently=False)

            us = User()
            us.first_name = first_name
            us.last_name = last_name
            us.email = email
            us.username = email
            us.set_password(password)
            p.user = us
            us.save()
            p.save()
            return redirect('hostel:login')
    else:
        form = RegForm()

    return render(request, 'hostel/reg_student.html', {'form': form})


def signin(request):
    print(request.method)
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        print(username, " ", password, " ", user)
        if user is not None:
            if user.userProfile.isVerified:
                auth.login(request, user)
                return redirect('hostel:dashboard')
            else:
                messages.error(request, 'Please Verify your email first...!!')
                return redirect('hostel:login')
        else:
            messages.error(request, 'Invalid email or password')
            return redirect('hostel:login')
    else:
        return render(request, 'hostel/login.html')


def open_dashboard_view(request):
    user_id = request.user.id
    userprofile = get_object_or_404(Person, user_id=user_id)
    user = get_object_or_404(User, id=user_id)

    context = {
        'user': user,
        'userprofile': userprofile
    }
    return render(request, 'hostel/dashboard.html', context)


def home(request):
    return render(request, "hostel/index.html")


def verification_view(request, username, uid):
    user = User.objects.get(username=username)
    if user:
        if user.userProfile.UID == uid:
            print(user.userProfile.isVerified)
            user.userProfile.isVerified = True
            user.userProfile.save()
            print(user.userProfile.isVerified)
            return redirect('hostel:login')
    raise Http404("Not Verified")


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()

            """Rehashing of password is done using update_session_auth_hash"""

            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            return redirect('hostel:dashboard')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'hostel/changepassword.html', {
        'form': form
    })


def postlogin(request):
    user = request.user
    print(user.email)
    userprofile = model_to_dict(user.user_profile)
    user = model_to_dict(user)
    return render(request, 'hostel/dashboard.html', {'user': user, 'userprofile': userprofile})


def logout_view(request):
    logout(request)
    return render(request, 'hostel/login.html')


def update(request):
    user = get_object_or_404(User, id=request.user.id)
    userprofile = get_object_or_404(Person, user_id=request.user.id)
    form = RegForm(request.POST or None, instance=userprofile)
    form1 = EditProfileForm(request.POST or None, instance=user)
    # print('FORM!!!!!!!!!!!!!!!!!!',form)
    if request.method == 'POST':
        print('i am here')
    if form.is_valid() and form1.is_valid():
        print('goodbye')
        user = form1.save()
        profile = form.save(commit=False)
        profile.user = user
        profile.save()
        return redirect('postlogin')
    else:
        print(form.errors, form1.errors)
    return render(request, 'hostel/update.html', {
        'form': form,
        'form1': form1
    })
